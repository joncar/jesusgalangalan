<?php $this->load->view('includes/template/header'); ?>
<section>
    <div class="sect-gap grey">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title style1">
                        <h2>El meu blog</h2>
                        <p> últimes notícies interessants </p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="blog-list">
                        <?= $output ?>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/template/footer'); ?>
