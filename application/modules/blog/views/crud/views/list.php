<?php if(!empty($list)): ?>    
    <?php foreach($list as $num_row => $p): ?>
            <div class="blogstyle blacky">
                <div class="parallax" style="background:url(<?= base_url('blog/'.$p->foto) ?>) no-repeat scroll 100% 0% / 100% 100% #000;"></div>
                <div class="blog-meta">
                    <h3><a href="<?= base_url('blog/'. toURL($p->id.'-'.$p->titulo)) ?>" title=""> <?= $p->titulo ?>  </a></h3>
                    <p>
                        <?= $p->texto ?>
                    </p>
                    <a href="<?= base_url('blog/'. toURL($p->id.'-'.$p->titulo)) ?>" title="" class="readmore">Llegir Més</a>
                    <ul>
                        <li><a href="#" title=""><i class="ti-instagram"></i></a></li>
                        <li><a href="#" title=""><i class="ti-twitter-alt"></i></a></li>
                        <li><a href="#" title=""><i class="ti-google"></i></a></li>
                        <li><a href="#" title=""><i class="ti-facebook"></i></a></li>
                    </ul>
                </div>
            </div>        
    <?php endforeach ?>     
<?php endif; ?>
