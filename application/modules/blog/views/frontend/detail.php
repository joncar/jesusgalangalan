<?php $this->load->view('includes/template/header'); ?>
<section>
    <div class="sect-gap grey no-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title style1">
                        <h2>Detall de la Noticia</h2>
                        <p><?= $detail->titulo ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="single-img">
            <img src="<?= $detail->foto ?>" alt="" />
        </div>
    </div>
</section>

<section>
    <div class="sect-gap grey">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="about">
                        <?= $detail->texto ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/template/footer'); ?>
