<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb7.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Detall de la Noticia</div>
                <div class="sub-title" >Titol noticia</div>
            </div>
        </div>
        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="../index.html">Inici</a></li>
                <li><a href="blog.html">Noticia</a></li>
                <li class="current"><a href="#">Detall de la noticia</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!--Blog Container-->
<section id="blog-section">
    <div class="inner-container container">
        <div class="post-main-container col-md-8">
            <!-- Post boxes -->
            <div class="post-box">
                <a class="post-img-box" href="#">
                    <img src="<?= $detail->foto ?>" alt="<?= $detail->titulo ?>" class="post-img">
                </a>
                <div class="post-b-sec">
                    <div class="post-title-box">
                        <a href="#" class="post-title"><?= $detail->titulo ?></a>
                    </div>
                    <div class="post-meta clearfix">
                        <div class="post-date"><i class="fa fa-calendar"></i> <?= strftime('%d, %B, %Y',strtotime($detail->fecha)); ?></div>
                        <div class="post-author"><i class="fa fa-edit"></i> By : <a href="#"><?= $detail->user ?></a></div>
                        <div class="post-category"><i class="fa fa-folder-open"></i><a href="<?= base_url('blog') ?>">Noticias</a></div>
<!--                        <div class="post-comment"><i class="fa fa-comments-o"></i> 10 <a href="#">Comments</a></div>-->
                    </div>
                    <div class="post-desc">
                        <?= $detail->texto ?>
                        </p>
                    </div>

                    <div class="post-tags">
                        <ul class="list-inline">
                            <?php foreach(explode(',',$detail->tags) as $t): ?>
                                <li><a href="<?= base_url('blog') ?>?direccion=<?= $t ?>"><?= $t ?></a></li>                  
                            <?php endforeach ?>
                        </ul>
                    </div>

                    <!-- Comment Box -->
<!--                    <div class="comments-container">-->
<!--                        <h3>3 <b>Comments</b></h3>-->
<!---->
                        <!-- Comment Boxes -->
<!--                        <div class="comment-box-container">-->
<!--                            <div class="comment-box">-->
<!--                                <div class="user-img">-->
<!--                                    <img src="../assets/img/users/1.jpg" alt="">-->
<!--                                </div>-->
<!--                                <div class="comment-info">-->
<!--                                    <div class="user-name">Lori <span>Bell</span></div>-->
<!--                                    <div class="comment-date">Jan 29, 2015</div>-->
<!--                                    <a href="#" class="reply fa fa-reply"></a>-->
<!--                                </div>-->
<!--                                <div class="comment-text">-->
<!--                                    Thanks for your great article. This article is so useful and help me to get more information about Themeforest.-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="comment-box-container">-->
                                <!-- Comment Box -->
<!--                                <div class="comment-box">-->
<!--                                    <div class="user-img">-->
<!--                                        <img src="../assets/img/users/2.jpg" alt="">-->
<!--                                    </div>-->
<!--                                    <div class="comment-info">-->
<!--                                        <div class="user-name">Sara <span>Lopez</span></div>-->
<!--                                        <div class="comment-date">Feb 1, 2015</div>-->
<!--                                        <a href="#" class="reply fa fa-reply"></a>-->
<!--                                    </div>-->
<!--                                    <div class="comment-text">-->
<!--                                        Thanks for your great article. This article is so useful and help me to get more information about Themeforest.-->
<!--                                    </div>-->
<!--                                </div>-->
                                <!-- Comment Box -->
<!--                                <div class="comment-box">-->
<!--                                    <div class="user-img">-->
<!--                                        <img src="../assets/img/users/3.jpg" alt="">-->
<!--                                    </div>-->
<!--                                    <div class="comment-info">-->
<!--                                        <div class="user-name">Chris <span>Jordan</span></div>-->
<!--                                        <div class="comment-date">Feb 3, 2015</div>-->
<!--                                        <a href="#" class="reply fa fa-reply"></a>-->
<!--                                    </div>-->
<!--                                    <div class="comment-text">-->
<!--                                        Thanks for all the comments, everyone! This article is so useful and help me to get more information about Themeforest.-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                    <!-- End of Comment Box -->

                    <!-- Comment Form -->
                    <!--<div class="comment-form-container">
                        <h3>Write a Comment</h3>
                        <div class="desc">Please fill all the fields of the below form and let us know what you are thinking about this post.</div>
                        <form class="comment-form">
                            <div class="field-row">
                                <input type="text" name="name" placeholder="Full Name :" required>
                            </div>
                            <div class="field-row">
                                <input type="email" name="email" placeholder="Email :" required>
                            </div>
                            <div class="field-row">
                                <input type="text" name="phone" placeholder="Phone :">
                            </div>
                            <div class="field-row">
                                <textarea name="comment" placeholder="Your Comment :" required></textarea>
                            </div>
                            <div class="field-row">
                                <input type="submit" value="Submit" class="btn btn-default">
                            </div>
                        </form>
                    </div>-->
                    <!-- End of Comment Form -->

                </div>
            </div>

        </div>

        <!--Sidebar Section-->
        <aside id="sidebar" class="col-md-4">
            <!-- Text Widget -->
            <!--<div class="widget widget_text">
                <h3 class="side-title"><span>Text</span> Widget</h3>
                <div class="textwidget">
                    Text widget can be used for putting text, images and some other elements in the widget areas. As an example you can add your short description about your hotel and add your logo in this area. Also you can add some useful information like notification in this area.
                </div>
            </div>-->

            <!-- Archive Widget -->
            <?php if($relacionados->num_rows()>0): ?>
                <div class="widget widget_archive">
                    <h3 class="side-title">Recientes</h3>
                    <ul>
                        <?php foreach($relacionados->result() as $e): ?>
                            <li><a href="<?= $e->link ?>"><?= $e->titulo ?></a></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            <?php endif ?>
            <!-- Search Widget -->
            <div class="widget widget_search">
                <form class="search-form" method="get" action="<?= site_url('blog') ?>">
                    <label>
                        <span class="screen-reader-text">Buscar por:</span>
                        <input type="search" class="search-field" placeholder="Buscar" value="" name="direccion" title="Buscar palabras">
                    </label>
                    <input type="submit" class="search-submit" value="Search">
                </form>
            </div>

            <!-- Tag Cloud Widget -->
            <div class="widget widget_tag_cloud">
                <h3 class="side-title"><span>Tags</span></h3>
                <div class="tagcloud">
                    <?php foreach(explode(',',$detail->tags) as $t): ?>
                        <a href="<?= base_url('blog') ?>?direccion=<?= $t ?>"><?= $t ?></a>                        
                    <?php endforeach ?>                    
                    
                </div>
            </div>
        </aside>
    </div>
</section>
<!--End of Blog Container-->
