<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function conferencias(){
            $crud = $this->crud_function('','');       
            $crud->set_field_upload('fondo','img');     
            $this->loadView($crud->render());
        }

        function actuaciones(){
            $crud = $this->crud_function('','');
            $this->loadView($crud->render());
        }
    }
?>
