<section>
    <?php $this->load->view('includes/template/header'); ?>
    <section>
        <div class="sect-gap grey">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title style1">
                            <h2>Sobre jo</h2>
                            <p>Sóc llicenciat en dret per la Universitat de Barcelona des del 1988...</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="aboutus">
                            <p>
                                ...i des d'aleshores m'hi dedico professionalment. Sóc l'administrador del meu propi despatx d'advocats, des d'on hem atès una infinitat de casos amb els quals hem après, gaudit i sobretot crescut professionalment, però també personalment.
                                Els meus coneixements sobre el dret conviuen amb la meva afició al teatre i la interpretació, una fusió que em permet gaudir dels meus dos mons impartint conferències. Així, de la unió del món del dret i la meva vocació teatral va néixer la conferència "Primers auxilis en dret", una xerrada amena on explico amb humor què és el dret i què hem de saber sobre temes com els divorcis, les herències o els drets del consumidor.
                            Al dret i a les meves habilitats comunicatives se li suma la meva filosofia de la vida amb la meditació com a pilar; el mindfulness. D'aquí neix la segona conferència titulada "Lideratge amb humor", un taller que ofereixo a les empreses amb la finalitat d'apropar als treballadors algunes eines per aprendre a gaudir treballant.                        </p>
                            <ul>
                                <!--        <li><a href="#" title="">Hire Me </a></li> -->
                                <li><a href="#" title="">Advocat </a></li>
                                <p>
                                    Llicenciat en dret per la Universitat de Barcelona<br>
                                Advocat en exercici des de fa més de 20 anys, amb despatx professional a Igualada (Barcelona) i administrador de la societat jurídica Galán Advocats.                      </p><br><br>
                                <li><a href="#" title="">Coach </a></li>
                                <p>
                                Coach titulat per Co-Activi Coaching - CTI, Global Iberia.                      </p><br><br>
                                <li><a href="#" title="">Formació en mindfulness </a></li>
                                <p>
                                    Format en MBSR per l'Institut  és-minfulness de Barcelona.<br>
                                    Curs avançat en mindfulness.<br>
                                </p><br><br>
                                <li><a href="#" title="">Teatre </a></li>
                                <p>
                                    Membre del grup de teatre del col·legiat d'advocats.<br>
                                    Formació en interpretació i actuació.<br>
                                    Actor en diverses obres de teatre.<br>
                                Guionista i actor de l'obra el Treball Dignifica (Barcelona).                       </p><br><br>
                                <li><a href="#" title="">Humor </a></li>
                                <p>
                                Guionista i actor de monòlegs.                      </p>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id='tema1'>
        <div class="sect-gap blacky">
            <div class="parallax" style="background:url(http://jesusgalangalan.com/img/primers.jpg)"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title red">
                            <div class="title style1"><span>CONFERÈNCIA 1</span></div>
                            <h3>Primers Auxilis en Dret</h3>
                            <!--                        <span>We create events aiming to spear the voice for children and gather for support. Please update with our events and confirm your presence.</span>-->
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="project-list">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="project">
                                        <img src="<?= base_url() ?>img/lideratge.jpg" style="width:90px; height:90px;border-radius: 100px;">
                                        <h3>DRET <span>què és?</span></h3>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="project">
                                        <img src="<?= base_url() ?>img/lideratge_1.jpg" style="width:90px; height:90px;border-radius: 100px;">
                                        <h3>Separacions <span>i Divorcis</span></h3>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="project">
                                        <img src="<?= base_url() ?>img/lideratge_2.jpg" style="width:90px; height:90px;border-radius: 100px;">
                                        <h3>Herències <span>i testaments</span></h3>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="project">
                                        <img src="<?= base_url() ?>img/lideratge_3.jpg" style="width:90px; height:90px;border-radius: 100px;">
                                        <h3>BANCS  <span>i CONSUM</span></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="sect-gap grey">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title style1">
                        <h2>Temes</h2>
                        <!--                        <p> Web & Mobile Application </p>-->
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="work-list">
                        <div class="col-md-6">
                            <div class="work blacky">
                                <div class="parallax" style="background:url(http://jesusgalangalan.com/img/llei.jpg) no-repeat; background-position:0 0;"></div>
                                <div class="work-meta">
                                    <p><img src="<?= base_url() ?>img/lideratge.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                    <h3>Dret, què és?</h3>
                                    <span>Què són les lleis i per què serveixen.</span>
                                    <p>
                                        <!--                                    Nemo                                     Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                        <!--                                    enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="work blacky">
                                <div class="parallax" style="background:url(http://jesusgalangalan.com/img/divorcis.jpg) no-repeat; background-position:0 0; "></div>
                                <div class="work-meta">
                                    <p><img src="<?= base_url() ?>img/lideratge_1.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                    <h3>Separacions i divorcis</h3>
                                    <span>Què haig de fer per separar-me?</span>
                                    <p>
                                        <!--                                    Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="work blacky">
                                <div class="parallax" style="background:url(http://jesusgalangalan.com/img/herencias.jpg) no-repeat; background-position:0 0; "></div>
                                <div class="work-meta">
                                    <p><img src="<?= base_url() ?>img/lideratge_2.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                    <h3>Herències i testaments</h3>
                                    <span>Què és una herència?</span>
                                    <p>
                                        <!--                                    Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="work blacky">
                                <div class="parallax" style="background:url(http://jesusgalangalan.com/img/bancs.jpg) no-repeat; background-position:0 0; "></div>
                                <div class="work-meta">
                                    <p><img src="<?= base_url() ?>img/lideratge_3.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                    <h3>Bancs i consum</h3>
                                    <span>Drets basics per als consumidors</span>
                                    <p>
                                        <!--                                    Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id='tema2'>
    <div class="sect-gap blacky">
        <div class="parallax" style="background:url(http://jesusgalangalan.com/img/segons.jpg) no-repeat; background-position:0 0;"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title red">
                        <div class="title style1"><span>CONFERÈNCIA - TALLER 2</span></div>
                        <h3 style=" color: #e6c165">LIDERATGE AMB HUMOR</h3>
                        <!--                        <span>We create events aiming to spear the voice for children and gather for support. Please update with our events and confirm your presence.</span>-->
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="project-list">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="project" style=" border-left: 2px solid #e6c165;">
                                    <img src="<?= base_url() ?>img/lideratge_4.jpg" style="width:63px; height:63px;border-radius: 100px;">
                                    <h3>OBJECTIU <span style=" color:;">TREBALLAR GAUDINT <BR> TREBALLAR AMB VALORS</span></span></h3>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="project" style=" border-left: 2px solid #e6c165;">
                                    <img src="<?= base_url() ?>img/lideratge_5.jpg" style="width:63px; height:63px;border-radius: 100px;">
                                    <h3>MÈTODE <span style=" color:;">GESTIÓ D'EMOCIONS <BR> AUTOCONEIXEMENTS</span></h3>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="project" style=" border-left: 2px solid #e6c165;">
                                    <img src="<?= base_url() ?>img/lideratge_6.jpg" style="width:63px; height:63px;border-radius: 100px;">
                                    <h3>INSTRUMENTS<span style=" color:;"> MINDFULNESS <BR> HUMOR</span></h3>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="project" style=" border-left: 2px solid #e6c165;">
                                    <img src="<?= base_url() ?>img/lideratge_7.jpg" style="width:63px; height:63px;border-radius: 100px;">
                                    <h3>RESULTAT <span style=" color:;">FELICITAT <BR> ÈXIT PERSONAL</span></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<section>
<div class="sect-gap grey">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title style1">
                    <h2>Temes</h2>
                    <!--                        <p> Lideratge amb humor </p>-->
                </div>
            </div>
            <div class="col-md-12">
                <div class="work-list">



                    <div class="col-md-6">
                        <div class="work blacky">
                            <div class="parallax" style="background:url(http://jesusgalangalan.com/img/objectius.jpg) no-repeat; background-position:0 0; "></div>
                            <div class="work-meta">
                                <p><img src="<?= base_url() ?>img/lideratge_4.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                <h3>OBJECTIU</h3>
                                <span style=" color: #e65651;">TREBALLANT GAUDINT<BR> TREBALLAR AMB VALORS</span>
                                <p>
                                    <!--                                    Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                </p>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="work blacky">
                            <div class="parallax" style="background:url(http://jesusgalangalan.com/img/instruments.jpg) no-repeat; background-position:0 0; "></div>
                            <div class="work-meta">
                                <p><img src="<?= base_url() ?>img/lideratge_5.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                <h3>MÈTODE</h3>
                                <span style=" color: #e65651;">GESTIÓ D'EMOCIONS <BR> AUTOCONEIXEMENT</span>
                                <p>
                                    <!--                                    Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="work blacky">
                            <div class="parallax" style="background:url(http://jesusgalangalan.com/img/metodos.jpg) no-repeat; background-position:0 0; "></div>
                            <div class="work-meta">
                                <p><img src="<?= base_url() ?>img/lideratge_6.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                <h3>INSTRUMENTS</h3>
                                <span style=" color: #e65651;">MINDFULNESS <BR> HUMOR</span>
                                <p>
                                    <!--                                    Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="work blacky">
                            <div class="parallax" style="background:url(http://jesusgalangalan.com/img/feliz.jpg) no-repeat; background-position:0 0; "></div>
                            <div class="work-meta">
                                <p><img src="<?= base_url() ?>img/lideratge_7.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                <h3>RESULTAT</h3>
                                <span style=" color: #e65651;">FELICITAT <BR> ÈXIT PERSONAL</span>
                                <p>
                                    <!--                                    Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<?php $this->load->view('includes/template/footer'); ?>