<div class="main-menu">
    <div class="container full">
        <div class="row">

            <div class="col-md-6">
                <!--<a href="<?= base_url() ?>p/conferencies#tema2">-->
                <div class="fancy-header">

                    <img src="<?= base_url() ?>img/956x780.jpg" alt="">
                    <div class="fixed-title">
                        <p style="margin-bottom:10px;">
                            <a href="<?= base_url('main/traduccion/ca') ?>" style="color:white; text-decoration: none">CAT</a> |
                            <a href="<?= base_url('main/traduccion/es') ?>" style="color:white; text-decoration: none">ESP</a>
                        </p>
                        <a href="<?= base_url('p/conferencies.html') ?>" style="border:0; padding:0">
                            <h2>Hola, Sóc en</h2>
                            <h1>Jesús Galán</h1>
                            <p>Advocat, Coach i Conferenciant</p>
                        </a>
                    </div>
                </div>	 <!--</a>-->
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="fancy-nav">
                            <img src="<?= base_url() ?>img/1.jpg" alt="">
                            <ul>
                                <li><a href="<?= base_url() ?>p/conferencies.html#tema1" title=""><i class="ti-announcement"></i></a></li>
                                <li><a href="<?= base_url() ?>p/conferencies.html#tema1" title="">Conferències</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="fancy-nav">
                            <img src="<?= base_url() ?>img/2.jpg" alt="">
                            <ul>
                                <li><a href="<?= base_url() ?>blog.html" title=""><i class="ti-calendar"></i></a></li>
                                <li><a href="<?= base_url() ?>blog.html" title="">Blog</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="fancy-nav">
                            <img src="<?= base_url() ?>img/3.jpg" alt="">
                            <ul>
                                <li><a href="<?= base_url() ?>p/galeria.html" title=""><i class="ti-gallery"></i></a></li>
                                <li><a href="<?= base_url() ?>p/galeria.html" title="">Galeria</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="fancy-nav">
                            <img src="<?= base_url() ?>img/4.jpg" alt="">
                            <ul>
                                <li><a href="<?= base_url() ?>p/contacte.html" title=""><i class="ti-marker-alt"></i></a></li>
                                <li><a href="<?= base_url() ?>p/contacte.html" title="">Contacte</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
    <div class="container full">
        <div class="row">
            <div class="bottom-info style1">
                <?php foreach($this->db->get_where('conferencias',array('mostrar'=>1))->result() as $r): ?>
                    <div class="col-md-4">
                        <div class="info-box">
                                <a href='<?= base_url('p/conferencies') ?>#tema1'>
                                    <img src="<?= base_url('img/'.$r->fondo) ?>" alt="" />

                                </a>
                                <div class="info-overlay">
                                    <a href='<?= base_url('p/conferencies').'/'.$r->anchor ?>'>
                                        <i class="<?= $r->icono ?>"></i>
                                        <p style="font-size:22px; font-weight:900;font-family:'Lato'"><?= $r->titulo ?></p>
                                    </a>
                                    <a href="javascript:void(0)" class="readmore popupInfoBoxButton" style="font-family:16px/44px 'Noticia Text'; width: 194px; margin: 20px 0 5px 0;">Próximas actuaciones</a><br/>
                                    <button class="popupInfoBoxButton2" type="button" style="background:transparent; border:0px; color:#e65650; font-size:14px; font-family:'Noticia Text'">Actuaciones realizadas</button>
                                </div>
                                <div class="popupInfoBox">
                                    <button class="popupInfoBoxClose" type="button"><i class="fa fa-remove"></i></button>
                                    <h1>Próximas Actuaciones</h1>
                                    <?php foreach($this->db->get_where('actuaciones',array('fecha >='=>date("Y-m-d"),'conferencias_id'=>$r->id))->result() as $p): ?>
                                        <p style="text-align: center;">
                                            Data: <?= strftime('%d de %B',strtotime($p->fecha)) ?><br />
                                            Hora: <?= $p->lugar ?><br/>
                                            Població: <?= $p->poblacion ?><br/>
                                        </p>
                                        <hr style="width:30px;">
                                    <?php endforeach ?>
                                </div>

                                <div class="popupInfoBox2">
                                    <button class="popupInfoBoxClose2" type="button"><i class="fa fa-remove"></i></button>
                                    <h1>Actuaciones realizadas</h1>
                                    <?php foreach($this->db->get_where('actuaciones',array('fecha <'=>date("Y-m-d"),'conferencias_id'=>$r->id))->result() as $p): ?>
                                        <p style="text-align: center;">
                                            Data: <?= strftime('%d de %B',strtotime($p->fecha)) ?><br />
                                            Hora: <?= $p->lugar ?><br/>
                                            Població: <?= $p->poblacion ?><br/>
                                        </p>
                                        <hr style="width:30px;">
                                    <?php endforeach ?>
                                </div>


                            </div>
                            <div>

                            
                        </div>
                    </div>
                <?php endforeach ?>  
                                
                <div class="col-md-4">
                    <div class="info-box">
                        <img src="img/7.jpg" alt="">
                        <div class="info-overlay">
                            <i class="ti-location-pin"></i>
                            <p><a href="mailto:info@jesusgalangalan.com" style="color:inherit"> info@jesusgalangalan.com </a></p>
                            <p><a href="tel:+34938031869" style="color:inherit"> +34-938-031-869  </a></p>
                        </div>
                    </div>	
                </div>
            </div>
        </div>
    </div>
</section>
<footer>        
    <div class="copyright">
        <p>Copyright &copy; 2018 Jesús Galán Galán</p>
    </div>
</footer>