<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb2.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Política de Privacitat</div>

            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="../index.html">Inici</a></li>
                <li class="current"><a href="#">Privacitat</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!--Welcome Section-->
<section id="welcome-section" class="simple">
    <div class="inner-container container">
        <div class="ravis-title-t-2">


        </div>
        <div class="content" style=" text-align: justify">

            <div class="h4"><span>POLÍTICA DE PRIVACIDAD</span></div>
            Jesus Galan Galan informa als usuaris del lloc web sobre la seva política respecte el tractament i protecció de les dades de caràcter personal dels usuaris i clients que puguin ser recaudades per la navegació o contractació de serveis a través del seu lloc web.
            En aquest sentit, Jesus Galan Galan garantitza el cumpliment de la normativa vigent en materia de protecció de dades personals, reflexada a la Ley Orgánica 15/1999 de 13 de desembre, de Protecció de Dades de Caràcter Personal i al Real Decreto 1720/2007, de 21 desembre, pel que s'aprova el Reglament de Desenvolupament de la LOPD.
            L'ús d'aquesta web implica l'acceptació d'aquesta política de privacitat.<br><br>



            <div class="h4"><span>RECOLLIDA, FINALITAT I TRACTAMENTS DE DADES</span></div>
            Jesus Galan Galan té el deure d'informar als usuaris del seu espai web sobre la recollida de dades de caràcter personal que puguin dur-se a terme, ja sigui mitjançant l'enviament de correu electrònic o al cumplimentar els formularis inclosos al lloc web. En aquest sentit, Jesus Galan Galan serà considerada com a responsable de les dades recaudades a través dels mitjans anteriorment descrits.
            Ahora Jesus Galan Galan informa als seus usuaris que la finalitat del tractament de les dades recollides contempla: L'atenció de sol·licituds realitzades pels usuaris, la inclusió en l'agenda de contactes, la prestació de serveis, la gestió de la relació comercial i altres finalitats (INDICAR)
             Les operacions, gestions i procediments tècnics que es realitzin de forma automatitzada o no automatitzada i que possibilitin la recollida, l'emmagatzematge, la modificació, la transferència i altres accions sobre dades de caràcter personal, tenen la consideració de tractament de dades personals.
             Totes les dades personals, que siguin recollides a través del lloc web de Jesus Galan Galan, i per tant tingui la consideració de tractament de dades de caràcter personal, seran incorporades als fitxers declarats a l'Agència Espanyola de Protecció de Dades per Jesus Galan Galan.<br>
            <br>



            <div class="h4"><span>COMUNICACIÓ D'INFORMACIÓ A TERCERS</span></div>
            Jesus Galan Galan informa als seus usuaris que les seves dades personals no seràn cedidos a terceres organizacions, amb l'excepció que la cessió de dades aquest emparada en una obligació legal o quan la prestació d'un servei impliqui la necessitat d'una relació contractual amb un encarregat de tractament. En aquest últim cas, només es durà a terme la cessió de dades al tercer quan Jesus Galan Galan disposi del consentiment exprés de l'usuari.


            <br><br>
            <div class="h4"><span>DRETS DELS USUARIS</span></div>
            La Ley Orgánica 15/1999, de 13 de desembre, de Protecció de Dades de Caràcter Personal concedeix a les persones interessades la possibilitat d'exercir una sèrie de drets relacionats amb el tractament de les seves dades personals.
            En la mesura que les dades de l'usuari són objecte de tractament per part de Jesus Galan Galan. Els usuaris podran exercir els drets d'accés, rectificació, cancel·lació i oposició d'acord amb el que preveu la normativa legal vigent en matèria de protecció de dades personals.
            Per fer ús de l'exercici d'aquests drets, l'usuari haurà de dirigir-se mitjançant comunicació escrita, aportant documentació que acrediti la seva identitat (DNI o passaport), a la següent adreça: Empresa A, Carrer: X No I, Codi postal: Z, Ciutat: V, Província: W o l'adreça que sigui substituïda en el Registre General de Protecció de Dades. Aquesta comunicació haurà de reflectir la següent informació: Nom i cognoms de l'usuari, la petició de sol·licitud, el domicili i les dades acreditatius.
            L'exercici de drets haurà de ser realitzat pel propi usuari. Això no obstant, podran ser executats per una persona autoritzada com a representant legal de l'autoritzat. En aquest cas, s'haurà d'aportar la documentació que acrediti aquesta representació de l'interessat. <br>

            <br>

        </div>
    </div>
</section>






    </div>

<?php $this->load->view('includes/template/footer'); ?>

<!--End of Footer Section-->
