<section>
    <?php $this->load->view('includes/template/header'); ?>

<section>
    <div class="sect-gap grey">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title style1">

                        <h2>Sobre mi</h2>
                        <p>Soy licenciado en derecho por la Universitat de Barcelona desde 1988...</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="aboutus">
                        <p>
                            ... y desde entonces me dedico profesionalmente. Soy el administrador de mi propio despacho de abogados, desde donde he atendido una infinidad de casos a través de los cuales hemos aprendido, disfrutado y sobretodo crecido profesionalmente, pero también personalmente. Mis conocimientos sobre el derecho conviven con mi afición al teatro y la interpretación, una fusión que me permite disfrutar de mis dos mundos impartiendo conferencias. Así, de la unión del mundo del derecho y mi vocación teatral nació la conferencia "Primeros auxilios en derecho", una charla amena donde explico con humor qué es el derecho y qué tenemos que saber sobre temas como los divorcios, las herencias o los derechos del consumidor. Al derecho y a mis habilidades comunicativas se le suma mi filosofía de la vida con la meditación como pilar; el mindfulness. De aquí nace la segunda conferencia titulada "Liderazgo con humor", un taller que ofrezco a las empresas con la finalidad de acercar a los trabajadores algunas herramientas para aprender a disfrutar trabajando.                        </p>
                        <ul>
                            <!--     	<li><a href="#" title="">Hire Me </a></li> -->
                            <li><a href="#" title="">Abogado </a></li>
                            <p>
                                Licenciado en derecho por la Universitat de Barcelona<br>
                                Abogado en ejercicio desde hace mas de 20 años, con despacho profesional en Igualada (Barcelona) y administrador de la sociedad jurídica Galán Advocats.                      </p><br><br>

                            <li><a href="#" title="">Coach </a></li>
                            <p>
                                Coach titulado por Co-Activi Coaching - CTI, Global Iberia.                      </p><br><br>

                            <li><a href="#" title="">Formación en mindfulness </a></li>
                            <p>
                                Formado en MBSR por el Institut  és-minfulness de Barcelona.<br>
                                Curso avanzado en mindfulness.<br>
                                                    </p><br><br>

                            <li><a href="#" title="">Teatro </a></li>
                            <p>
                                Miembro del grupo de teatro del colegio de abogados.<br>
                                Formación en interpretación y actuación.<br>
                                Actor en varias obras de teatro.<br>
                                Guionista y actor de la obra el Treball Dignifica (Barcelona).                       </p><br><br>

                            <li><a href="#" title="">Humor </a></li>
                            <p>
                                Guionista y actor de monólogos.                      </p>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id='tema1'>
    <div class="sect-gap blacky">
        <div class="parallax" style="background:url(http://jesusgalangalan.com/img/primers.jpg)"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title red">
                        <div class="title style1"><span>CONFERENCIA 1</span></div>
                        <h3>Primers Auxilis en Dret</h3>
<!--                        <span>We create events aiming to spear the voice for children and gather for support. Please update with our events and confirm your presence.</span>-->
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="project-list">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="project">
                                    <img src="<?= base_url() ?>img/lideratge.jpg" style="width:90px; height:90px;border-radius: 100px;">
                                    <h3>DERECHO <span>qué es?</span></h3>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="project">
                                    <img src="<?= base_url() ?>img/lideratge_1.jpg" style="width:90px; height:90px;border-radius: 100px;">
                                    <h3>Separaciones <span>y Divorcios</span></h3>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="project">
                                    <img src="<?= base_url() ?>img/lideratge_2.jpg" style="width:90px; height:90px;border-radius: 100px;">
                                    <h3>Herencias <span>y testamentos</span></h3>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="project">
                                    <img src="<?= base_url() ?>img/lideratge_3.jpg" style="width:90px; height:90px;border-radius: 100px;">
                                    <h3>BANCOS  <span>y CONSUMO</span></h3>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

<section>
    <div class="sect-gap grey">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title style1">
                        <h2>Temas</h2>
<!--                        <p> Web & Mobile Application </p>-->
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="work-list">



                        <div class="col-md-6">
                            <div class="work blacky">

                                <div class="parallax" style="background:url(http://jesusgalangalan.com/img/llei.jpg) no-repeat; background-position:0 0;"></div>
                                <div class="work-meta">
                                    <p><img src="<?= base_url() ?>img/lideratge.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                    <h3>Derecho, qué es?</h3>
                                    <span>Qué son las leyes y para qué sirven.</span>
                                    <p>
    <!--                                    Nemo                                     Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
    <!--                                    enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                    </p>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="work blacky">
                                <div class="parallax" style="background:url(http://jesusgalangalan.com/img/divorcis.jpg) no-repeat; background-position:0 0; "></div>
                                <div class="work-meta">
                                    <p><img src="<?= base_url() ?>img/lideratge_1.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                    <h3>Separaciones y divorcios</h3>
                                    <span>Qué tengo que hacer para separarme?</span>
                                    <p>
    <!--                                    Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="work blacky">
                                <div class="parallax" style="background:url(http://jesusgalangalan.com/img/herencias.jpg) no-repeat; background-position:0 0; "></div>
                                <div class="work-meta">
                                    <p><img src="<?= base_url() ?>img/lideratge_2.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                    <h3>Herencias y testamentos</h3>
                                    <span>Qué es una herencia?</span>
                                    <p>
    <!--                                    Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="work blacky">
                                <div class="parallax" style="background:url(http://jesusgalangalan.com/img/bancs.jpg) no-repeat; background-position:0 0; "></div>
                                <div class="work-meta">
                                    <p><img src="<?= base_url() ?>img/lideratge_3.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                    <h3>Bancos y consumo</h3>
                                    <span>Derechos básicos para los consumidores</span>
                                    <p>
    <!--                                    Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id='tema2'>
    <div class="sect-gap blacky">
        <div class="parallax" style="background:url(http://jesusgalangalan.com/img/segons.jpg) no-repeat; background-position:0 0;"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title red">
                        <div class="title style1"><span>CONFERENCIA - TALLER 2</span></div>
                        <h3 style=" color: #e6c165">LIDERAZGO CON HUMOR</h3>
<!--                        <span>We create events aiming to spear the voice for children and gather for support. Please update with our events and confirm your presence.</span>-->
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="project-list">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="project" style=" border-left: 2px solid #e6c165;">
                                    <img src="<?= base_url() ?>img/lideratge_4.jpg" style="width:63px; height:63px;border-radius: 100px;">
                                    <h3>OBJETIVO 
                                        <span style=" color:;">TRABAJANDO DISFRUTANDO 
                                            <BR> TRABAJAR CON VALORES
                                        </span>
                                    </h3>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="project" style=" border-left: 2px solid #e6c165;">
                                    <img src="<?= base_url() ?>img/lideratge_5.jpg" style="width:63px; height:63px;border-radius: 100px;">
                                    <h3>MÉTODO <span style=" color:;">GESTIÓN DE EMOCIONES <BR> AUTOCONOCIMIENTO</span></h3>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="project" style=" border-left: 2px solid #e6c165;">
                                    <img src="<?= base_url() ?>img/lideratge_6.jpg" style="width:63px; height:63px;border-radius: 100px;">
                                    <h3>INSTRUMENTOS <span style=" color: ;">MINDFULNESS <BR> HUMOR</span></h3>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="project" style=" border-left: 2px solid #e6c165;">
                                    <img src="<?= base_url() ?>img/lideratge_7.jpg" style="width:63px; height:63px;border-radius: 100px;">
                                    <h3>RESULTADO <span style=" color:;">FELICIDAD <BR> ÉXITO PERSONAL</span></h3>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

<section>
    <div class="sect-gap grey">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title style1">
                        <h2>Temas</h2>
<!--                        <p> Lideratge amb humor </p>-->
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="work-list">
                        <div class="col-md-6">
                            <div class="work blacky">
                                <div class="parallax" style="background:url(http://jesusgalangalan.com/img/objectius.jpg) no-repeat; background-position:0 0; "></div>
                                <div class="work-meta">
                                    <p><img src="<?= base_url() ?>img/lideratge_4.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                    <h3>OBJETIVO</h3>
                                    <span style=" color: #e65651;">TRABAJAR DISFRUTANDO<BR> TRABAJANDO CON VALORES</span>
                                    <p>
    <!--                                    Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="work blacky">
                                <div class="parallax" style="background:url(http://jesusgalangalan.com/img/instruments.jpg) no-repeat; background-position:0 0; "></div>
                                <div class="work-meta">
                                    <p><img src="<?= base_url() ?>img/lideratge_5.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                    <h3>MÉTODO</h3>
                                    <span style=" color: #e65651;">GESTIÓN DE EMOCIONES <BR> AUTOCONOCIMIENTO</span>
                                    <p>
    <!--                                    Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="work blacky">
                                <div class="parallax" style="background:url(http://jesusgalangalan.com/img/metodos.jpg) no-repeat; background-position:0 0; "></div>
                                <div class="work-meta">
                                    <p><img src="<?= base_url() ?>img/lideratge_6.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                    <h3>INSTRUMENTOS</h3>
                                    <span style=" color: #e65651;">MINDFULNESS <BR> HUMOR</span>
                                    <p>
    <!--                                    Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="work blacky">
                                <div class="parallax" style="background:url(http://jesusgalangalan.com/img/feliz.jpg) no-repeat; background-position:0 0; "></div>
                                <div class="work-meta">
                                    <p><img src="<?= base_url() ?>img/lideratge_7.jpg" style="width:90px; height:90px;border-radius: 100px;margin-bottom: 26px"></p>
                                    <h3>RESULTADO</h3>
                                    <span style=" color: #e65651;">FELICIDAD <BR> ÉXITO PERSONAL</span>
                                    <p>
    <!--                                    Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem Nemo enim ipsam volupipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.-->
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/template/footer'); ?>
