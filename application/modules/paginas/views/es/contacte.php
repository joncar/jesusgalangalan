<?php $this->load->view('includes/template/header'); ?>
<section>
    <div class="sect-gap grey">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title style1">
                        <h2>Contacta conmigo</h2>
                        <p>Enviame un mensaje si tienes alguna duda o quieres contratar mis servicios</p>
                    </div>
                </div>
                <div class="contact-form" id='contacto'>                    
                    <div class="col-md-12">
                        <div>
                            <?php if(!empty($_SESSION['msj'])){
                                echo $_SESSION['msj'];
                                unset($_SESSION['msj']);
                            }
                            ?>
                        </div>
                        <div class="loader" style="display:none">
                            <img src="<?= base_url() ?>img/loader.gif" alt="" />
                        </div>
                    </div>
                    <form action='<?= base_url('paginas/frontend/contacto') ?>' method="post">
                        <div class="col-md-6">
                            <label>
                                <i class="ti-user"></i>
                                <input type="text" name="name" placeholder="nom i Cognoms" id="txt-name" />
                            </label>

                        </div>
                        <div class="col-md-6">
                            <label>
                                <i class="ti-email"></i>
                                <input type="text" name="email" placeholder="Email" id="txt-email" />
                            </label>
                        </div>
                        <div class="col-md-12">
                            <label>
                                <i class="ti-receipt"></i>
                                <input type="text" name="tema" placeholder="Tema" id="txt-sub" />
                            </label>
                        </div>
                        <div class="col-md-12">
                            <textarea name="message" placeholder="Deixa el missatge" id="txt_msg" ></textarea>
                            <div class="g-recaptcha" data-sitekey="6Lf5-jwUAAAAANyNdtHgVlkTMhbItU32JkDvrkjH"></div>
                            <div class="btn-submit">
                                <button><i class="ti-email"></i>Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/template/footer'); ?>
