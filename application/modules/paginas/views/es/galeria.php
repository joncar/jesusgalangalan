<?php $this->load->view('includes/template/header'); ?>
<section>
    <div class="sect-gap grey">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title style1">
                        <h2>Galería</h2>
                        <p>Una imagen vale más que mil palabras</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="portfolioFilter">
                        <ul>
                            <li><a href="#" data-filter=".1cat" class="current"><i class="ti-support"></i>Primeros Auxilios en Derecho</a></li>
                            <li><a href="#" data-filter=".2cat"><i class="ti-comments-smiley"></i>Liderazgo con Humor</a></li>                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container full">
            <div class="row">
                <div class="portfolio-wrapper merged">
                    <?php foreach($this->db->get('categorias_fotos')->result() as $c): ?>                    
                        <?php foreach($this->db->get_where('fotos',array('categorias_fotos_id'=>$c->id))->result() as $f): ?>                            
                            <div class="col-md-3 <?= $c->id ?>cat main">
                                <div class="portfolio">
                                    <img src="<?= base_url('img/entorno/'.$f->foto) ?>" alt="image">
                                </div>
                            </div>
                        <?php endforeach ?>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('includes/template/footer'); ?>
