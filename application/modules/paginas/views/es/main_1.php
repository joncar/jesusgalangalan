<div class="main-menu">
    <div class="container full">
        <div class="row">

            <div class="col-md-6">
                <a href="<?= base_url() ?>p/conferencies#tema2">
                <div class="fancy-header">

                    <img src="<?= base_url() ?>img/956x780.jpg" alt="">
                    <div class="fixed-title">
                        <h2>Hola, Soy</h2>
                        <h1>Jesús Galán</h1>
                        <p>Abogado, Coach y Conferenciante</p>
                    </div>
                </div>	 </a>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="fancy-nav">
                            <img src="<?= base_url() ?>img/1.jpg" alt="">
                            <ul>
                                <li><a href="<?= base_url() ?>p/conferencies.html" title=""><i class="ti-announcement"></i></a></li>
                                <li><a href="<?= base_url() ?>p/conferencies.html" title="">Conferencias</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="fancy-nav">
                            <img src="<?= base_url() ?>img/2.jpg" alt="">
                            <ul>
                                <li><a href="<?= base_url() ?>blog.html" title=""><i class="ti-calendar"></i></a></li>
                                <li><a href="<?= base_url() ?>blog.html" title="">Blog</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="fancy-nav">
                            <img src="<?= base_url() ?>img/3.jpg" alt="">
                            <ul>
                                <li><a href="<?= base_url() ?>p/galeria.html" title=""><i class="ti-gallery"></i></a></li>
                                <li><a href="<?= base_url() ?>p/galeria.html" title="">Galería</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="fancy-nav">
                            <img src="<?= base_url() ?>img/4.jpg" alt="">
                            <ul>
                                <li><a href="<?= base_url() ?>p/contacte.html" title=""><i class="ti-marker-alt"></i></a></li>
                                <li><a href="<?= base_url() ?>p/contacte.html" title="">Contacto</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section>
    <div class="container full">
        <div class="row">
            <div class="bottom-info style1">
                                    <div class="col-md-4">
                        <div class="info-box">
                                <a href="<?= base_url() ?>p/conferencies#tema1">
                                    <img src="<?= base_url() ?>img/2d12c-sin-titulo-2.jpg" alt="">

                                </a>
                                <div class="info-overlay">
                                    <a href="<?= base_url() ?>p/conferencies/#tema1">
                                        <i class="ti-support"></i>
                                        <p style="font-size:22px; font-weight:900;font-family:'Lato'">PRIMEROS AUXILIOS <br>EN DERECHO</p>
                                    </a>
                                    <button class="popupInfoBoxButton" type="button" style="background:transparent; border:0px; color:white; font-size:16px; font-family:'Noticia Text'">Próximas actuaciones</button>

                                </div>
                                <div class="popupInfoBox">
                                    <button class="popupInfoBoxClose" type="button"><i class="fa fa-remove"></i></button>
                                    <h1>Próximas Actuaciones</h1>
                                    <p style="text-align: center;">Fecha:<br>Sitio:<br>Población:<br><br></p>
<p style="text-align: center;"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFIAAAAFCAYAAADWp7vUAAAAAXNSR0IArs4c6QAAAClJREFUOBFj/A8EDKOA4hBgotiEUQPAITAakFRKCKMBORqQVAoBKhkDALhbBAbN7ggTAAAAAElFTkSuQmCC" width="5" height="1"></p>                                </div>

                            </div>
                            <div>

                            
                        </div>
                    </div>
                                    <div class="col-md-4">
                        <div class="info-box">
                                <a href="<?= base_url() ?>p/conferencies#tema1">
                                    <img src="<?= base_url() ?>img/f3e0e-sin-titulo-2.jpg" alt="">

                                </a>
                                <div class="info-overlay">
                                    <a href="<?= base_url() ?>p/conferencies/#tema2">
                                        <i class="ti-comments-smiley"></i>
                                        <p style="font-size:22px; font-weight:900;font-family:'Lato'"> CONFERENCIA - TALLER<br>  LIDERAZGO CON HUMOR</p>
                                    </a>
                                    <button class="popupInfoBoxButton" type="button" style="background:transparent; border:0px; color:white; font-size:16px; font-family:'Noticia Text'">Próximas actuaciones</button>

                                </div>
                                <div class="popupInfoBox">
                                    <button class="popupInfoBoxClose" type="button"><i class="fa fa-remove"></i></button>
                                    <h1>Próximas Actuaciones</h1>
                                    <p style="text-align: center;">Fecha:<br>Sitio:<br>Población:</p>                                </div>

                            </div>
                            <div>

                            
                        </div>
                    </div>
                                
                <div class="col-md-4">
                    <div class="info-box">
                        <img src="img/7.jpg" alt="">
                        <div class="info-overlay">
                            <i class="ti-location-pin"></i>
                            <p><a href="mailto:info@jesusgalangalan.com" style="color:inherit"> info@jesusgalangalan.com </a></p>
                            <p><a href="tel:+34938031869" style="color:inherit"> +34-938-031-869  </a></p>
                        </div>
                    </div>	
                </div>
            </div>
        </div>
    </div>
</section>