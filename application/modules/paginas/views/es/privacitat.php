<!--Breadcrumb Section-->
<section id="breadcrumb-section" data-bg-img="<?= base_url() ?>img/breadcrumb2.jpg">
    <div class="inner-container container">
        <div class="ravis-title">
            <div class="inner-box">
                <div class="title">Política de Privacidad</div>

            </div>
        </div>

        <div class="breadcrumb">
            <ul class="list-inline">
                <li><a href="../index.html">Inicio</a></li>
                <li class="current"><a href="#">Privacidad</a></li>
            </ul>
        </div>
    </div>
</section>
<!--End of Breadcrumb Section-->

<!--Welcome Section-->
<section id="welcome-section" class="simple">
    <div class="inner-container container">
        <div class="ravis-title-t-2">


        </div>
        <div class="content" style=" text-align: justify">

            <div class="h4"><span>POLÍTICA DE PRIVACIDAD</span></div>
            Jesus Galan Galan informa a los usuarios del sitio web sobre su política respecto del tratamiento y protección de los datos de carácter personal de los usuarios y clientes que puedan ser recabados por la navegación o contratación de servicios a través de su sitio web.
            En este sentido, Jesus Galan Galan garantiza el cumplimiento de la normativa vigente en materia de protección de datos personales, reflejada en la Ley Orgánica 15/1999 de 13 de diciembre, de Protección de Datos de Carácter Personal y en el Real Decreto 1720/2007, de 21 diciembre, por el que se aprueba el Reglamento de Desarrollo de la LOPD.
            El uso de esta web implica la aceptación de esta política de privacidad.<br><br>



            <div class="h4"><span>RECOGIDA, FINALIDAD Y TRATAMIENTOS DE DATOS</span></div>
            Jesus Galan Galan tiene el deber de informar a los usuarios de su sitio web acerca de la recogida de datos de carácter personal que pueden llevarse a cabo, bien sea mediante el envío de correo electrónico o al cumplimentar los formularios incluidos en el sitio web. En este sentido, Jesus Galan Galan será considerada como responsable de los datos recabados mediante los medios anteriormente descritos.
            A su vez Jesus Galan Galan informa a los usuarios de que la finalidad del tratamiento de los datos recabados contempla: La atención de solicitudes realizadas por los usuarios, la inclusión en la agenda de contactos, la prestación de servicios, la gestión de la relación comercial y otras finalidades (INDICAR)
            Las operaciones, gestiones y procedimientos técnicos que se realicen de forma automatizada o no automatizada y que posibiliten la recogida, el almacenamiento, la modificación, la transferencia y otras acciones sobre datos de carácter personal, tienen la consideración de tratamiento de datos personales.
            Todos los datos personales, que sean recogidos a través del sitio web de Jesus Galan Galan, y por tanto tenga la consideración de tratamiento de datos de carácter personal, serán incorporados en los ficheros declarados ante la Agencia Española de Protección de Datos por Jesus Galan Galan.<br>
            <br>



            <div class="h4"><span>COMUNICACIÓN DE INFORMACIÓN A TERCEROS</span></div>
            Jesus Galan Galan informa a los usuarios de que sus datos personales no serán cedidos a terceras organizaciones, con la salvedad de que dicha cesión de datos este amparada en una obligación legal o cuando la prestación de un servicio implique la necesidad de una relación contractual con un encargado de tratamiento. En este último caso, solo se llevará a cabo la cesión de datos al tercero cuando Jesus Galan Galan disponga del consentimiento expreso del usuario.


            <br><br>
            <div class="h4"><span>DERECHOS DE LOS USUARIOS</span></div>
            La Ley Orgánica 15/1999, de 13 de diciembre, de Protección de Datos de Carácter Personal concede a los interesados la posibilidad de ejercer una serie de derechos relacionados con el tratamiento de sus datos personales.
            En tanto en cuanto los datos del usuario son objeto de tratamiento por parte de Jesus Galan Galan. Los usuarios podrán ejercer los derechos de acceso, rectificación, cancelación y oposición de acuerdo con lo previsto en la normativa legal vigente en materia de protección de datos personales.
            Para hacer uso del ejercicio de estos derechos, el usuario deberá dirigirse mediante comunicación escrita, aportando documentación que acredite su identidad (DNI o pasaporte), a la siguiente dirección: Jesus Galan Galan, Calle: X No Y, Código postal: Z, Ciudad: V, Provincia: W o la dirección que sea sustituida en el Registro General de Protección de Datos. Dicha comunicación deberá reflejar la siguiente información: Nombre y apellidos del usuario, la petición de solicitud, el domicilio y los datos acreditativos.
            El ejercicio de derechos deberá ser realizado por el propio usuario. No obstante, podrán ser ejecutados por una persona autorizada como representante legal del autorizado. En tal caso, se deberá aportar la documentación que acredite esta representación del interesado.            <br>

            <br>

        </div>
    </div>
</section>






    </div>

<?php $this->load->view('includes/template/footer'); ?>

<!--End of Footer Section-->
