<section id='footer'>
    <div class="container full">
        <div class="row">
            <div class="bottom-info">
                <div class="col-md-5">
                    <div class="info-box">
                        <img src="http://jesusgalangalan.com/img/jesus.jpg" alt="" />
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="info-box">
                                <img src="http://jesusgalangalan.com/img/7.jpg" alt="" />
                                <div class="info-overlay">
                                    <i class="ti-direction"></i>
                                    <p>On estem?</p>
                                </div>
                            </div>	
                        </div>
                        <div class="col-md-6">
                            <div class="info-box">
                                <img src="http://jesusgalangalan.com/img/tel.jpg" alt="" />
                                <div class="info-overlay">
                                    <i class="ti-mobile"></i>
                                    <p>+34-938-031-869 </p>
                                </div>
                            </div>	
                        </div>
                        <div class="col-md-6">
                            <div class="info-box">
                                <img src="http://jesusgalangalan.com/img/mail.jpg" alt="" />
                                <div class="info-overlay">
                                    <i class="ti-email"></i>
                                    <p>info@jesusgalangalan.com</p>
                                </div>
                            </div>	
                        </div>
                        <div class="col-md-6">
                            <div class="info-box">
                                <img src="http://jesusgalangalan.com/img/news.jpg" alt="" />
                                <div class="info-overlay">
                                    <i class="ti-marker-alt"></i>
                                    <p>Suscriu-te al butlletí</p>
                                </div>
                            </div>	
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer>    	
    <div class="copyright">
        <p>Copyright &copy; 2018 Jesús Galán Galán</p>
    </div>
</footer>
