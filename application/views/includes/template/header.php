<header>
        <div class="container full">
        	<div class="row">
            	<div class="col-md-12">
                	<div class="inner-header_bg">
                    	<img src="http://jesusgalangalan.com/img/8.jpg" alt="" />
                    </div>
                	<div class="fixed-title inner">                    	
                        <div class="top-close">
                                <a href="<?= site_url() ?>" title="" class="btn-close"><i class="fa fa-close"></i></a>
                        </div>
                        <h2>Hola, Soc en</h2>
                        <h1> Jesús Galán Galán </h1>
                        <p>Advocat, Coach i Conferenciant</p>
                        <ul class="social">
                            <li><a href="https://www.facebook.com/jesus.galan2" title="" class="fb"><i class="ti-facebook"></i></a></li>
                            <li><a href="https://plus.google.com/116007606271911150260" title="" class="gplus"><i class="ti-google"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UC0-bboKoAWymXeIhWR4rs6w?spfreload=10" title="" class="reddit"><i class="ti-youtube"></i></a></li>
                            <li><a href="https://twitter.com/galanabogados" title="" class="drible"><i class="ti-twitter-alt"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
