<script type="text/javascript" src="<?= base_url() ?>js/template/jquery2.1.1.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/app.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/template/jquery.isotope.js"></script>
<script src='https://www.google.com/recaptcha/api.js?hl=ca'></script>
<script type="text/javascript">

    "use strict";

    (function ($) {
        jQuery(window).load(function () {
            jQuery(".styles-list").mCustomScrollbar();
        });
    })(jQuery);

    jQuery(document).ready(function ($) {
        $('.btn-mov a').on('click', function () {
            jQuery('.styles-list').toggleClass('up');
            jQuery('.btn-mov').toggleClass('up');
            return false;
        });
    });

    $(window).load(function () {
        var $container = jQuery('.portfolio-wrapper');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });

        $('.portfolioFilter ul li a').click(function () {
            $('.portfolioFilter .current').removeClass('current');
            $(this).addClass('current');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });
    })


</script>

<script type="text/javascript">

    "use strict";

    $(".testimonial-carousel").owlCarousel({
        margin: 30,
        singleItem: false,
        items: 2,
        nav: false,
        autoplay: true,
        autoplayTimeout: 3000,
        smartSpeed: 2000,
        loop: true,
        dots: true,
        responsive: {
            0: {items: 1},
            480: {items: 1},
            768: {items: 2},
            1200: {items: 2},
        }
    });

    $('#circle-one').circliful();
    $('#circle-two').circliful();
    $('#circle-three').circliful();
    $('#circle-four').circliful();

    (function ($) {
        $(window).load(function () {
            $(".styles-list").mCustomScrollbar();
        });
    })(jQuery);

    $(document).ready(function ($) {
        $('.btn-mov a').on('click', function () {
            $('.styles-list').toggleClass('up');
            $('.btn-mov').toggleClass('up');
            return false;
        });
    });

</script>
<script>
    (function ($) {
        $(window).load(function () {
            $(".styles-list").mCustomScrollbar();
        });
    })(jQuery);

    $(document).ready(function ($) {
        $('.btn-mov a').on('click', function () {
            $('.styles-list').toggleClass('up');
            $('.btn-mov').toggleClass('up');
            return false;
        });
    });



</script>
<script>

    "use strict";

    (function ($) {
        $(window).load(function () {
            $(".styles-list").mCustomScrollbar();
        });
    })(jQuery);

    $(document).ready(function ($) {
        $('.btn-mov a').on('click', function () {
            $('.styles-list').toggleClass('up');
            $('.btn-mov').toggleClass('up');
            return false;
        });

        $(document).on('click','.popupInfoBox, .popupInfoBoxButton',function(){
            $(this).parents('.info-box').find('.popupInfoBox').addClass('active');
        });

        $(document).on('click','.popupInfoBoxClose',function(e){
            e.stopPropagation();                  
            $(this).parents('.popupInfoBox').attr('class','popupInfoBox'); 
        }); 

        $(document).on('click','.popupInfoBox2, .popupInfoBoxButton2',function(){
            $(this).parents('.info-box').find('.popupInfoBox2').addClass('active');
        });

        $(document).on('click','.popupInfoBoxClose2',function(e){
            e.stopPropagation();                  
            $(this).parents('.popupInfoBox2').attr('class','popupInfoBox2'); 
        }); 
    });


    /*$(".btn-submit button").on('click', function () {

        var name = jQuery("#txt-name").val();
        var email = jQuery("#txt-email").val();
        var subj = jQuery("#txt-sub").val();
        var msg = jQuery("#txt_msg").val();
        var action = "act_contact";
        var data = "action=" + action + "&name=" + name + "&email=" + email + "&subj=" + subj + "&msg=" + msg;

        $.ajax({
            'type': "POST",
            'url': 'http://usertheme.com/templates/mr-resume/contact.php',
            'data': data,
            beforeSend: function () {
                jQuery(".loader").show();
            },
            success: function (response) {
                jQuery(".loader").hide();
                jQuery(".error-msg").show();
                jQuery(".error-msg").html(response);
            }
        });

        return false;
    });*/


    

</script>
<?php 
if(!empty($css_files) && !empty($js_files)): ?>
<?php foreach($js_files as $file): ?>
<script src="<?= $file ?>"></script>
<?php endforeach; ?>                
<?php endif; ?>
