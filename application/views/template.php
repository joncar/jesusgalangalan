<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title><?= empty($title)?'JesusGalanGalan':$title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link rel="icon" href="<?= base_url('icono.jpg') ?>" type="image/x-icon"/>    
        <link rel="shortcut icon" href="<?= base_url('icono.jpg') ?>" type="image/x-icon"/>

        <link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Noticia+Text:400,700,400italic,700italic' rel='stylesheet' type='text/css'>       
        <link href="https://fonts.googleapis.com/css?family=Rock+Salt" rel="stylesheet">
        <?php 
        if(!empty($css_files) && !empty($js_files)):
        foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
        <?php endforeach; ?>               
        <?php endif; ?>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/bootstrap.min.css"  />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/themify-icons.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/owl.carousel.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/prettyPhoto.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/jquery.mCustomScrollbar.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/style.css" />
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/template/responsive.css" />
    </head>

    <body>        
        <?php $this->load->view($view); ?>     
        <?php $this->load->view('includes/template/modals'); ?>   
        <?php $this->load->view('includes/template/scripts'); ?>
        <?php $this->load->view('predesign/aviso-cookies'); ?>
    </body>

</html>
